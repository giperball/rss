package ru.tusur.rss.services;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import ru.tusur.rss.domain.Frame;
import ru.tusur.rss.domain.FrameNI;
import ru.tusur.rss.domain.NodeActivity;
import ru.tusur.rss.domain.XBeeNode;

import java.nio.IntBuffer;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class SerialPortReaderTest {

    static final String FAKE_PORT_NAME = "/dev/ttyUSB-test";

    @Test
    public void testReaderFullinOne() throws Exception {
        PortService.SerialPortReader spr = new PortService.SerialPortReader();
        byte[] buffer = new byte[]{0x7E, 0x00, 0x0A, (byte) 0x88, 0x01, 0x4E, 0x49, 0x00,
                0x65, 0x6E, 0x64, 0x64, 0x32, 0x12};
        spr.createFrame(buffer, FAKE_PORT_NAME);

        assertEquals(PortService.synchronizedQueue.size(), 1);

    }

    @Mock
    private static SerialPort port;

    @InjectMocks
    private PortService portService;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() {

        /*PortService ps = new PortService();
        for (Frame frame : PortService.synchronizedQueue) {
        }*/
        PortService.synchronizedQueue.clear();
    }

    @Test
    public void testReaderTwoInOne() throws Exception {
        PortService.SerialPortReader spr = new PortService.SerialPortReader();
        byte[] buffer = new byte[]{0x7E, 0x00, 0x0A, (byte) 0x88, 0x01, 0x4E, 0x49,
                0x00, 0x65, 0x6E, 0x64, 0x64, 0x32, 0x12,
                0x7E, 0x00, 0x0A, (byte) 0x88, 0x01, 0x4E, 0x49,
                0x00, 0x65, 0x6E, 0x64, 0x64, 0x32, 0x12};

        PortService.setPort(port);
        Mockito.when(port.readBytes(buffer.length)).thenReturn(buffer);
        //spr.createFrame(buffer, FAKE_PORT_NAME);
        spr.serialEvent(new SerialPortEvent("/dev/ttyUSB-test", SerialPortEvent.RXCHAR, buffer.length));
        assertEquals(PortService.synchronizedQueue.size(), 2);
    }

    @Test
    public void testCheck() throws Exception {

        PortService.SerialPortReader spr = new PortService.SerialPortReader();
        byte[] buffer = new byte[]{0x7E, 0x00, 0x0A, (byte) 0x88, 0x01, 0x4E, 0x49,
                0x00, 0x65, 0x6E, 0x64, 0x64, 0x32, 0x12,
                0x7E, 0x00, 0x0A, (byte) 0x88, 0x01, 0x4E, 0x49,
                0x00, 0x65, 0x6E, 0x64, 0x64, 0x32, 0x12};
        PortService.setPort(port);
        Mockito.when(port.readBytes(buffer.length)).thenReturn(buffer);
        spr.serialEvent(new SerialPortEvent("/dev/ttyUSB-test", SerialPortEvent.RXCHAR, buffer.length));

        for (Frame frame : PortService.synchronizedQueue) {
            assertTrue(frame.check());
        }

    }

    @Test
    public void testCommandData() throws Exception {

        PortService.SerialPortReader spr = new PortService.SerialPortReader();
        byte[] buffer = new byte[]{0x7E, 0x00, 0x0A, (byte) 0x88, 0x01, 0x4E, 0x49,
                0x00, 0x65, 0x6E, 0x64, 0x64, 0x32, 0x12,
                0x7E, 0x00, 0x0A, (byte) 0x88, 0x01, 0x4E, 0x49,
                0x00, 0x65, 0x6E, 0x64, 0x64, 0x32, 0x12};
        PortService.setPort(port);
        Mockito.when(port.readBytes(buffer.length)).thenReturn(buffer);
        spr.serialEvent(new SerialPortEvent("/dev/ttyUSB-test", SerialPortEvent.RXCHAR, buffer.length));

        for (Frame frame : PortService.synchronizedQueue) {
            assertTrue(frame.check());
        }

        for (Frame frame : PortService.synchronizedQueue) {

            Byte[] data = frame.getCommandData();

            assertTrue(frame.byteToString(data).equalsIgnoreCase("endd2"));
            assertTrue(frame.getAtCommand().equalsIgnoreCase("NI"));

            if (frame.getAtCommand().equalsIgnoreCase("NI")) {
                new FrameNI(frame);
            }
        }

    }

    @Test
    public void testNiFromNI() throws Exception {

        PortService.SerialPortReader spr = new PortService.SerialPortReader();
        byte[] buffer = new byte[]{0x7E, 0x00, 0x0A, (byte) 0x88, 0x01, 0x4E, 0x49,
                0x00, 0x65, 0x6E, 0x64, 0x64, 0x32, 0x12,
                0x7E, 0x00, 0x0A, (byte) 0x88, 0x01, 0x4E, 0x49,
                0x00, 0x65, 0x6E, 0x64, 0x64, 0x32, 0x12};
        PortService.setPort(port);
        Mockito.when(port.readBytes(buffer.length)).thenReturn(buffer);
        spr.serialEvent(new SerialPortEvent("/dev/ttyUSB-test", SerialPortEvent.RXCHAR, buffer.length));

        for (Frame frame : PortService.synchronizedQueue) {
            assertTrue(frame.check());
        }

        for (Frame frame : PortService.synchronizedQueue) {
            assertTrue(frame.getNiFromNI().equalsIgnoreCase("endd2"));
        }
    }

    @Test
    public void testNiFromND() throws Exception {

        PortService.SerialPortReader spr = new PortService.SerialPortReader();
        byte[] buffer = new byte[]{0x7E, 0x00, 0x20, (byte) 0x88, 0x01, 0x4E, 0x44, 0x00, (byte) 0xFF, (byte) 0xFE,
                0x00, 0x13, (byte) 0xA2, 0x00, 0x40, (byte) 0xB3, (byte) 0xF4, 0x50, 0x48, 0x65, 0x61, 0x64, 0x6C,
                0x65, 0x73, 0x73, 0x00, (byte) 0xFF, (byte) 0xFE, 0x01, 0x00, (byte) 0xC1, 0x05, 0x10, 0x1E, (byte) 0xE0};
        PortService.setPort(port);
        Mockito.when(port.readBytes(buffer.length)).thenReturn(buffer);
        spr.serialEvent(new SerialPortEvent("/dev/ttyUSB-test", SerialPortEvent.RXCHAR, buffer.length));

        for (Frame frame : PortService.synchronizedQueue) {
            assertTrue(frame.check());
        }

        for (Frame frame : PortService.synchronizedQueue) {
            assertTrue(frame.getNiFromND().equalsIgnoreCase("Headless"));
        }

    }

    @Test
    public void testReaderFullInTwoSeparate() throws Exception {
        PortService.SerialPortReader spr = new PortService.SerialPortReader();
        byte[] buffer = new byte[]{0x7E, 0x00, 0x0A, (byte) 0x88, 0x01, 0x4E, 0x49, 0x00,
                0x65, 0x6E, 0x64, 0x64, 0x32, 0x12};

        byte[] buffer2 = new byte[]{0x7E, 0x00, 0x0A, (byte) 0x88, 0x01, 0x4E, 0x49, 0x00,
                0x65, 0x6E, 0x64, 0x64, 0x32, 0x12};


        spr.createFrame(buffer, FAKE_PORT_NAME);
        spr.createFrame(buffer2, FAKE_PORT_NAME);

        assertEquals(PortService.synchronizedQueue.size(), 2);

    }

    @Test
    public void testReaderGarbageWithOne() throws Exception {
        PortService.SerialPortReader spr = new PortService.SerialPortReader();
        byte[] buffer = new byte[]{0x64, 0x32, 0x7E, 0x00, 0x0A, (byte) 0x88, 0x01, 0x4E, 0x49, 0x00,
                0x65, 0x6E, 0x64, 0x64, 0x32, 0x12};
        spr.createFrame(buffer, FAKE_PORT_NAME);

        assertEquals(PortService.synchronizedQueue.size(), 1);

    }

    @Test
    public void testStrange() throws Exception {
        //0x7E001997010013A20040B3F450FFFE4953000100000703FF03FF03FFD4

        PortService.SerialPortReader spr = new PortService.SerialPortReader();
        byte[] buffer = new byte[]{0x7E, 0x00, 0x19, (byte) 0x97, 0x01, 0x00, 0x13, (byte) 0xA2, 0x00, 0x40, (byte) 0xB3,
                (byte) 0xF4, 0x50, (byte) 0xFF, (byte) 0xFE, 0x49, 0x53, 0x00, 0x01, 0x00, 0x00, 0x07, 0x03, (byte) 0xFF,
                0x03, (byte) 0xFF, 0x03, (byte) 0xFF, (byte) 0xD4};
        PortService.setPort(port);
        Mockito.when(port.readBytes(buffer.length)).thenReturn(buffer);
        spr.serialEvent(new SerialPortEvent("/dev/ttyUSB-test", SerialPortEvent.RXCHAR, buffer.length));

        for (Frame frame : PortService.synchronizedQueue) {
            assertTrue(frame.check());
            System.out.println((frame.getRemoteAddr()));
        }
    }

    @Test
    public void testStrange2() throws Exception {
        //0x7E001997010013A20040B3F450FFFE4953000100000703FF03FF03FFD4

        PortService.SerialPortReader spr = new PortService.SerialPortReader();
        byte[] buffer = new byte[]{0x7E, 0x00, 0x0F, (byte) 0x97, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                (byte) 0xFF, (byte) 0xFE, 0x49, 0x53, 0x04, (byte) 0xCA};
        PortService.setPort(port);
        Mockito.when(port.readBytes(buffer.length)).thenReturn(buffer);
        spr.serialEvent(new SerialPortEvent("/dev/ttyUSB-test", SerialPortEvent.RXCHAR, buffer.length));

        for (Frame frame : PortService.synchronizedQueue) {
            assertTrue(frame.check());
            System.out.println((frame.getRemoteAddr()));
        }
    }

    @Test
    public void testCreateChk() {
        XBeeNode node = new XBeeNode();
        node.setIdBytes(new byte[]{0x00, 0x13, (byte) 0xA2, 0x00, 0x40, (byte) 0x93, 0x44, (byte) 0x92});
//        node.setIdBytes(new byte[]{0x00, 0x13 , (byte)0xA2 , 0x00 , 0x40 , (byte)0x93 , 0x44 , (byte)0xC9});
        byte[] result = PortService.createIScmd(node);
        System.out.println(PortService.getBytesAsHexString(result));
        IntBuffer buffer = Frame.getIntBuff(result);
        assertTrue(Frame.check(buffer));
    }

//    @Test
//    public void testDecodeFrame() throws Exception {
//        NodeActivity activity = new NodeActivity();
//        byte[] buffer = new byte[]{0x7E, 0x00, 0x16, (byte) 0x92, 0x00, 0x13, (byte) 0xa2, 0x00, 0x40, 0x4c, 0x0e, (byte) 0xbe, 0x61, 0x59,
//                0x01, 0x01, 0x00, 0x18, 0x03, 0x00, 0x10, 0x02, 0x2f, 0x01, (byte) 0xfe, 0x49};
//        activity.setResponseFrame(buffer);
//        activity.setAtCommand("IS");
//
//        assertEquals(activity.getAnalogMask(), 3);
//        assertEquals(activity.getEnabledAnalog(activity.getAnalogMask()), 2);
//        assertArrayEquals(activity.getAnalogSample(1), new byte[]{0x01, (byte) 0xfe});
//        assertArrayEquals(activity.getAnalogSample(2), new byte[]{0x02, 0x2f});
//
//        System.out.println(activity.decodeFrame());
//    }

    @Test
    public void testDecodeFrame2() throws Exception {
        NodeActivity activity = new NodeActivity();
        byte[] buffer = new byte[]{0x7E, 0x00, 0x1B, (byte)0x97, 0x01, 0x00, 0x13, (byte)0xA2, 0x00, 0x40, (byte)0x93,
                0x45, 0x2F, (byte)0xFF, (byte) 0xFE, 0x49, 0x53, 0x00, 0x01, 0x00, 0x38, 0x07, 0x00, 0x30, 0x01,
                (byte)0x9B, 0x00, (byte)0xCA, 0x00,(byte) 0xC1, 0x3B};
        activity.setResponseFrame(buffer);
        activity.setAtCommand("IS");

        assertEquals(activity.getAnalogMask(), 7);
        assertEquals(activity.getEnabledAnalog(activity.getAnalogMask()), 3);
        assertArrayEquals(activity.getAnalogSample(1), new byte[]{ 0x00,(byte) 0xC1});
        assertArrayEquals(activity.getAnalogSample(2), new byte[]{0x00, (byte)0xCA});
        assertArrayEquals(activity.getAnalogSample(3), new byte[]{0x01, (byte)0x9B});

        System.out.println(activity.decodeFrame());
    }

//    @Test
//    public void testISresponse


}