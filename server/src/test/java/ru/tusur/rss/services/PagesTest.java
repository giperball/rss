package ru.tusur.rss.services;

import com.google.common.base.Preconditions;
import org.apache.tools.ant.taskdefs.Sleep;
import org.apache.wicket.examples.repeater.Index;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import ru.tusur.rss.GateContext;
import ru.tusur.rss.wicket.MainPage;
import ru.tusur.rss.wicket.data.ServerHomePage;
import ru.tusur.rss.wicket.data.SortingDataPage;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = GateContext.class,
        loader = AnnotationConfigContextLoader.class)
@ActiveProfiles("test")
public class PagesTest  extends AbstractJUnit4SpringContextTests {

    private static WicketTester tester;

    @Test
    public void mainPageTest() throws Exception {
        tester.startPage(MainPage.class);
        tester.assertRenderedPage(Index.class);
    }




    @Test
    public void startTest() throws Exception {
        tester.startPage(ServerHomePage.class);
        tester.assertRenderedPage(ServerHomePage.class);
    }

    @Test
    public void getFilesTest() throws Exception {
        tester.startPage(ServerHomePage.class);
        tester.assertRenderedPage(ServerHomePage.class);
        tester.assertContains("\"class_img_files\\\">" + (new ServerHomePage()).getFiles() + "</h4>");
    }


    @Test
    public void scanFolderTest() throws Exception {
        ServerHomePage page = new ServerHomePage();
        String str = page.getFiles();
        assert str.isEmpty() || str.endsWith(";");
    }

    @Test
    public void titleTest() throws Exception {
        tester.startPage(ServerHomePage.class);
        tester.assertLabel("title", "Central server");
    }

    /*@Test
    public void lonLatTest() throws Exception {
        tester.startPage(McHomePage.class);
        tester.assertRenderedPage(McHomePage.class);
        for(int i = 0; i < 10; i++) {
            Thread.sleep(200);
            tester.assertContains("lon\">\\d*\\.\\d*\\<");
            tester.assertContains("lat\">\\d*\\.\\d*\\<");
        }
    }


    @Test
    public void scanFolderTest() throws Exception {
        tester.startPage(McHomePage.class);
        tester.assertRenderedPage(McHomePage.class);
        McHomePage page = new McHomePage();
        String str = page.scanFolder();
        assert str.isEmpty() || str.endsWith(";");
    }*/



    @Before
    public void init() {

        getTester();

    }

    WicketTester getTester() {

        if (tester == null) {
            WebApplication application = (WebApplication) applicationContext.getBean("wicketApplication");
            Preconditions.checkNotNull(application);
            tester = new WicketTester(application);
        }
        return tester;

//        if (tester == null) {
//            tester = new WicketTester();
//        }
//        return tester;
    }
}