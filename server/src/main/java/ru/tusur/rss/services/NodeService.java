package ru.tusur.rss.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.tusur.rss.dao.XBeeNodeRepository;
import ru.tusur.rss.domain.XBeeNode;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Vladimir Vagaytsev
 */
@Service("nodeService")
@DependsOn({"liquibase"})
public class NodeService {

    @Autowired
    PortService portService;

    @Autowired
    XBeeNodeRepository xBeeNodeRepository;

    private final Map<String, XBeeNode> nodes = new HashMap<>();



    @Scheduled(initialDelay = 1000, fixedDelayString = "${discover.delay}")
    public void discoverNodes(){
     portService.discoverNodes();
    }

    @Scheduled(initialDelay = 1000, fixedDelayString = "${is.request.delay}")
    public void isRequestDeleay(){
        portService.pollIsCmd(nodes);
    }

    @PostConstruct
    public void validate() {
        if (portService == null) {
            throw new IllegalStateException("portService == null");
        }

        initList();
    }

    private void initList() {
        nodes.clear();
        List<XBeeNode> list = (List) xBeeNodeRepository.findAll();
        for (XBeeNode xBeeNode : list) {
            nodes.put(xBeeNode.getZigBeeId(), xBeeNode);
        }
    }

    public boolean hasNode(String nodeID) {
        return nodes.keySet().contains(nodeID);
    }

    public XBeeNode getNode(String nodeID) {
        return nodes.get(nodeID);
    }

    public void save(XBeeNode xBeeNode) {
        xBeeNodeRepository.save(xBeeNode);
        initList();
    }

    public void delete(XBeeNode xBeeNode) {
        xBeeNodeRepository.delete(xBeeNode);
        initList();
    }
}