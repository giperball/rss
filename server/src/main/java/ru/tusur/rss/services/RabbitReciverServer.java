package ru.tusur.rss.services; /**
 * Created by giperball on 21.10.14.
 */

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import javafx.util.Pair;
import org.apache.wicket.util.string.StringList;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

class ChechTimerTask extends TimerTask{
    public void run()
    {
        RabbitReciverServer.mcs.clear();
    }

}

//@Service("rabbitReciverServer")
public class RabbitReciverServer extends Thread {
    public static HashMap<String, Pair<String, String> > mcs = new HashMap<String, Pair<String, String> >();
    private Timer timer = new Timer();
    public RabbitReciverServer() throws IOException {
    }

    @Override
    public void run() {
        try {
            timer.schedule(new ChechTimerTask(), 0, 3000);
            ConnectionFactory factory = new ConnectionFactory();
            factory.setUsername("guest");
            factory.setPassword("guest");
            factory.setVirtualHost("/");
            factory.setHost("127.0.0.1");
            factory.setPort(5672);
            Connection conn = factory.newConnection();
            Channel channel = conn.createChannel();
            String exchangeName = "myExchange1";
            String queueName = "myQueue1";
            String routingKey = "testRoute1";
            boolean durable = true;
            channel.exchangeDeclare(exchangeName, "direct", durable);
            channel.queueDeclare(queueName, durable, false, false, null);
            channel.queueBind(queueName, exchangeName, routingKey);
            QueueingConsumer consumer = new QueueingConsumer(channel);
            channel.basicConsume(queueName, false, consumer);
            boolean run = true;
            while (run) {
                QueueingConsumer.Delivery delivery;
                try {
                    delivery = consumer.nextDelivery();
                    handleDelivery(delivery);
                    channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                } catch (InterruptedException ie) {
                    continue;
                }
            }
            channel.close();
            conn.close();
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
    }

    private void handleDelivery(QueueingConsumer.Delivery delivery)
    {
        String delivery_text = new String(delivery.getBody());
        System.out.println("New delivery on mc " + delivery_text);
        if(delivery_text.startsWith("send_coords"))
        {
            String[] list = delivery_text.split(" ");
            if(list.length != 4)
                return;
            mcs.put(list[1], new Pair<String, String>(list[2], list[3]));
        }
    }
}

/*
public class RabbitReciver extends Thread {
    public RabbitReciver() throws IOException {
    }

    @Override
    public void run() {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setUsername("guest");
            factory.setPassword("guest");
            factory.setVirtualHost("/");
            factory.setHost("127.0.0.1");
            factory.setPort(5672);
            Connection conn = factory.newConnection();
            Channel channel = conn.createChannel();
            String exchangeName = "myExchange";
            String queueName = "myQueue";
            String routingKey = "testRoute";
            boolean durable = true;
            channel.exchangeDeclare(exchangeName, "direct", durable);
            channel.queueDeclare(queueName, durable, false, false, null);
            channel.queueBind(queueName, exchangeName, routingKey);
            QueueingConsumer consumer = new QueueingConsumer(channel);
            channel.basicConsume(queueName, false, consumer);
            boolean run = true;
            while (run) {
                QueueingConsumer.Delivery delivery;
                try {
                    delivery = consumer.nextDelivery();
                    System.out.println("New rabbit " + delivery.getBody());
                    channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                } catch (InterruptedException ie) {
                    continue;
                }
            }
            channel.close();
            conn.close();
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
    }
}
*/