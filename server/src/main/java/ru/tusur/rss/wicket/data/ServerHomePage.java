package ru.tusur.rss.wicket.data;

import com.google.common.base.Ticker;
import org.apache.wicket.ajax.AbstractDefaultAjaxBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.examples.WicketExamplePage;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.template.PackageTextTemplate;
import ru.tusur.rss.services.RabbitReciverServer;
import sun.misc.IOUtils;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Created by giperball on 21.10.14.
 */

public class ServerHomePage extends ExamplePage {
    private Label mcs_label;
    private Label title_label;
    private String mcs_str;
    private boolean checkbox1 = false; // uncheck
    private boolean checkbox2 = true; // checked by default

    /*public  ServerHomePage(PageParameters parameters) {

        add(new Label("img_files", getFiles()));

    }*/

    public String getFiles(){
        String result = "";
        File folder = new File("/var/tmp/central_server/tiny/img/");
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile())
                result += "/tiny/img/" + listOfFiles[i].getName() + ";";
        }
        return result;
    }

    public ServerHomePage() throws IOException {
        add(new Label("title", "Central server"));
        add(new Label("img_files", getFiles()));
/*        Form<?> form = new Form("form");
        final CheckBox chk1 = new CheckBox("checkbox1", new PropertyModel<Boolean>(this, "checkbox1"));
        final CheckBox chk2 = new CheckBox("checkbox2",
                new PropertyModel<Boolean>(this, "checkbox2"));*/
        //form.add(new RegistrationInputPanel("registration", regModel);
        /*form.add(chk1);
        form.add(chk2);
        form.add(new Button("register") {
            public void onSubmit() {
                System.out.println("checkbox1 : " + Boolean.toString(checkbox1) +
                                "checkbox2 : " + Boolean.toString(checkbox2) +
                        "ON SUUUUUUUBMIT");
            }
        });
        add(form);*/

        mcs_label = new Label("mcs", new PropertyModel(this, "mcs_str"));
        mcs_label.setOutputMarkupId(true);
        add(mcs_label);


        add(new AjaxFallbackLink("coord_id") {

                @Override
                public void onClick(AjaxRequestTarget target) {
                    String res = "";
                    Iterator<String> it = RabbitReciverServer.mcs.keySet().iterator();
                    while(it.hasNext())
                    {
                        String name = it.next();
                        res +=  name + " " +
                                RabbitReciverServer.mcs.get(name).getKey() + " " +
                                RabbitReciverServer.mcs.get(name).getValue() + " ";
                    }
                    if(res.length() > 0)
                        res = res.substring(0, res.length() - 1);

                    mcs_str = res;

                    if (target != null) {
                        target.add(mcs_label);
                    }
                }
            }
        );
    }

}
