package ru.tusur.rss.wicket.data;

/**
 * Created by giperball on 23.11.14.
 */
import javax.servlet.http.*;
import javax.servlet.ServletException;
import java.io.*;
import java.util.*;
import java.util.List;

import org.apache.commons.fileupload.*;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;

/**
 13    * Сервлет-обработчик формы загрзки файлов на сервер
 14    */
public class UploadFile extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            //Список загружаемых файлов
            List files = new ArrayList();
            //Список обычных параметров из HTML-формы
            Map params = new HashMap();
            //Инициализируем структуры files и params
            init(request, params, files);
            //Сохраняем файл на сервере
            save(files, params);

            rsyncStart();

            String site = "/" ;
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", site);

            response.setContentType("text/html; charset=windows-1251");
            final PrintWriter writer = response.getWriter();
            writer.println("Файл успешно загружен<br>");
            writer.println("<a href='" + request.getContextPath() + "/s2'>Загрузить еще >></a>");
            writer.close();
        }
        catch (FileUploadException fue) {
            fue.printStackTrace();
            throw new ServletException(fue);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new ServletException(e);
        }

    }

    private  void rsyncStart(){
        try {

            Runtime.getRuntime().exec("rsync -avr --delete /var/tmp/central_server/tiny/img/ /var/tmp/mc_server/tiny/img/");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void save(List files, Map params) throws IOException {
        try {
            for (Iterator i = files.iterator(); i.hasNext();) {
                FileItem item = (FileItem) i.next();
                //Файл, в который нужно произвести запись
                final File file = new File("/var/tmp/central_server/tiny/img/" + File.separator + UUID.randomUUID().toString() + item.getName());
                FileOutputStream fos = new FileOutputStream(file);
                fos.write(item.get());
                fos.close();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void init(HttpServletRequest request, Map params, List files) throws FileUploadException {
        DiskFileItemFactory factory = new DiskFileItemFactory();
        //Устанавливаем каталог для временных файлов
        factory.setRepository(new File("/tmp"));
        ServletFileUpload upload = new ServletFileUpload(factory);
        //Установим ограничение на размер загружаемого файла в битах
        upload.setSizeMax(50 * 1024);
        List items = upload.parseRequest(request);
        for (Iterator i = items.iterator(); i.hasNext();) {
            FileItem item = (FileItem) i.next();
            //Проверяем, является ли параметр обычным полем из HTML-формы,
            //если да, то помещаем в Map пару name=value...
            if (item.isFormField()) {
                params.put(item.getFieldName(), item.getString());
            }
            //... если нет, то конструируем объект AttachmentDataSource и
            //помещаем его в список прикрепленных файлов
            else {
                if (item.getSize() <= 0) continue;
                files.add(item);
            }
        }
    }
}