package ru.tusur.rss.wicket.data;

/**
 * Created by giperball on 24.11.14.
 */

import javax.servlet.http.*;
import javax.servlet.ServletException;
import java.io.*;
import java.util.*;

import org.apache.commons.fileupload.*;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.tusur.rss.services.RabbitProducerServer;

/**
 13    * Сервлет-обработчик формы загрзки файлов на сервер
 14    */
public class NewsServlet extends HttpServlet {

    RabbitProducerServer rabbitProducerServer = new RabbitProducerServer();
    public NewsServlet() throws IOException {

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        rabbitProducerServer.send(request.getParameter("news"));

        String site = "/" ;
        response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", site);

        response.setContentType("text/html; charset=windows-1251");


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        //rabbitProducer.send("news:" + request.getParameter("news"));

        String site = "/" ;
        response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", site);

        response.setContentType("text/html; charset=windows-1251");


    }
}