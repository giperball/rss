package ru.tusur.rss.wicket.data;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * Created by gipreball on 23.11.14.
 */

public class DeleteFile extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {



        String site = "/" ;
        response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", site);

        response.setContentType("text/html; charset=windows-1251");


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        new File("/var/tmp/central_server/"+request.getParameter("delete")).delete();
        rsyncStart();
        //rabbitProducer.send("news:" + request.getParameter("news"));

        String site = "/" ;
        response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", site);

        response.setContentType("text/html; charset=windows-1251");


    }

    private  void rsyncStart(){
        try {

            Runtime.getRuntime().exec("rsync -avr --delete /var/tmp/central_server/tiny/img/ /var/tmp/mc_server/tiny/img/");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}