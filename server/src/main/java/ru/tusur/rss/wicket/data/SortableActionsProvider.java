/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.tusur.rss.wicket.data;

import org.apache.wicket.examples.repeater.*;
import org.apache.wicket.extensions.markup.html.repeater.data.sort.SortOrder;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.IFilterStateLocator;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import ru.tusur.rss.dao.ActivitiesRepository;
import ru.tusur.rss.domain.NodeActivity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


/**
 * implementation of IDataProvider for contacts that keeps track of sort information
 * 
 * @author igor
 * 
 */
public class SortableActionsProvider extends SortableDataProvider<NodeActivity, String> implements IFilterStateLocator<ContactFilter>
{

//    @Autowired
//    @SpringBean
//    ActivitiesRepository activitiesRepository;

	private ContactFilter contactFilter = new ContactFilter();

	/**
	 * constructor
	 */
	public SortableActionsProvider()
	{
		// set default sort
		setSort("firstName", SortOrder.ASCENDING);
	}

//	protected ActivitiesRepository getContactsDB()
//	{
//		return GateDatabaseLocator.getDatabase();
//	}

    public ActivitiesRepository getRepo() {
        return GateDatabaseLocator.getDatabase();
    }

	@Override
	public Iterator<NodeActivity> iterator(long first, long count)
	{
		List<NodeActivity> contactsFound = (List) getRepo().ordered();
//		List<NodeActivity> contactsFound = getContactsDB().getIndex(getSort());

		return filterContacts(contactsFound).
			subList((int)first, (int)(first + count)).
			iterator();
	}

	private List<NodeActivity> filterContacts(List<NodeActivity> contactsFound)
	{
	    ArrayList<NodeActivity> result = new ArrayList<>();
	    Date dateFrom = contactFilter.getDateFrom();
	    Date dateTo = contactFilter.getDateTo();

	    for (NodeActivity activity : contactsFound)
	    {
		Date bornDate = new Date (activity.getUpdateTimeStamp());

		if(dateFrom != null && bornDate.before(dateFrom))
		{
		    continue;
		}

		if(dateTo != null && bornDate.after(dateTo))
		{
		    continue;
		}

		result.add(activity);
	    }

	    return result;
	}

	/**
	 * @see org.apache.wicket.markup.repeater.data.IDataProvider#size()
	 */
	@Override
	public long size()
	{
		return filterContacts((List) getRepo().findAll()).size();
//		return filterContacts(getContactsDB().getIndex(getSort())).size();
	}

	/**
	 * @see org.apache.wicket.markup.repeater.data.IDataProvider#model(Object)
	 */
	@Override
	public IModel<NodeActivity> model(NodeActivity object)
	{
		return new DetachableActivityModel(object);
	}

	@Override
	public ContactFilter getFilterState()
	{
	    return contactFilter;
	}

	@Override
	public void setFilterState(ContactFilter state)
	{
	    contactFilter  = state;
	}

	
}
