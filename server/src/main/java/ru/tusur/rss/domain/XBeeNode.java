package ru.tusur.rss.domain;

import javax.persistence.*;
import java.util.List;

/**
 * User: saturn
 * Date: 17.09.2014
 */
@Entity
@Table(name = "nodes")
/*@NamedQueries({
        @NamedQuery(name = "XBeeNode.findRevolverForNow",
                query = "select w from WindowStat w where w.createTimeStamp >= ? and w.createTimeStamp < ? and w.revolverId = ? and w.revWindowId = ?")
})*/
public class XBeeNode {

    @Id
    @Column(name = "zig_bee_id_64")
    public String zigBeeId;

    @Column(name = "node_name")
    public String nodeName;



    @Column(name = "create_time_stamp")
    public long createTimeStamp;

    @Column(name = "update_time_stamp")
    public long updateTimeStamp;

    @Column(name = "description")
    public String description;

    @Column(name = "lat")
    public double lat;

    @Column(name = "lon")
    public double lon;

    @Column(name = "zig_bee_id_bt")
    public byte[] idBytes;

    @Column(name = "is_tracked")
    public boolean isTracked = true;

    @OneToMany (cascade={CascadeType.ALL},fetch=FetchType.EAGER)
    @JoinColumn(name="node_id", referencedColumnName="zig_bee_id_64")
    public List<NodeActivity> activities;

    public XBeeNode() {

    }

    public XBeeNode(
            String nodeId,
            byte[] idBytes,
            long createTimeStamp,
            long updateTimeStamp,
            List<NodeActivity> activities
    ) {
        this.zigBeeId = nodeId;
        this.createTimeStamp = createTimeStamp;
        this.updateTimeStamp = updateTimeStamp;
        this.activities = activities;
        this.idBytes = idBytes;
    }

    public byte[] getIdBytes() {
        return idBytes;
    }

    public void setIdBytes(byte[] idBytes) {
        this.idBytes = idBytes;
    }

    public boolean isTracked() {
        return isTracked;
    }

    public void setTracked(boolean isTracked) {
        this.isTracked = isTracked;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public List<NodeActivity> getActivities() {
        return activities;
    }

    public void setActivities(List<NodeActivity> activities) {
        this.activities = activities;
    }

    public long getUpdateTimeStamp() {
        return updateTimeStamp;
    }

    public void setUpdateTimeStamp(long updateTimeStamp) {
        this.updateTimeStamp = updateTimeStamp;
    }

    public long getCreateTimeStamp() {
        return createTimeStamp;
    }

    public void setCreateTimeStamp(long createTimeStamp) {
        this.createTimeStamp = createTimeStamp;
    }

    public String getZigBeeId() {
        return zigBeeId;
    }

    public void setZigBeeId(String zigBeeId) {
        this.zigBeeId = zigBeeId;
    }
}
