package ru.tusur.rss.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by saturn on 22.09.14.
 */
public class FrameIS extends ResponseFrame{

    private static final Logger log = LoggerFactory.getLogger(FrameIS.class);

    public FrameIS(Frame frame){
        super(frame);
    }

}
