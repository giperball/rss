package ru.tusur.rss.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.wicket.util.io.IClusterable;
import ru.tusur.rss.services.PortService;

import javax.persistence.*;
import java.text.NumberFormat;
import java.util.Arrays;

/**
 * Created by saturn on 17.09.14.
 */




@Entity
@Table(name = "activities")
@NamedQuery(name = "NodeActivity.ordered", query = "select n from NodeActivity n order by n.updateTimeStamp desc ")
public class NodeActivity implements IClusterable {

    @Column
    @Id
    @GeneratedValue
    @JsonIgnore
    private int id;

    @Column(name = "frame_type")
    private int frameType;

    @Column(name = "node_id", updatable = true)
    @JsonIgnore
    private String nodeId;

    @Column(name = "at_command")
    public String atCommand;

    @Column(name = "update_time_stamp")
    private long updateTimeStamp;

    @Column(name = "port")
    private String portName;

    @Column(name = "request_frame")
    private String requestFrame;

    @Column(name = "node_name")
    private String nodeName;

    @Column(name = "response_frame")
    private byte[] responseFrame;

    public NodeActivity() {
    }

    public NodeActivity(
            long updateTimeStamp
    ) {
        this.updateTimeStamp = updateTimeStamp;
    }


    public String getAtCommand() {
        return atCommand;
    }

    public void setAtCommand(String atCommand) {
        this.atCommand = atCommand;
    }

    public int getFrameType() {
        return frameType;
    }

    public void setFrameType(int frameType) {
        this.frameType = frameType;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getUpdateTimeStamp() {
        return updateTimeStamp;
    }

    public void setUpdateTimeStamp(long updateTimeStamp) {
        this.updateTimeStamp = updateTimeStamp;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getPortName() {
        return portName;
    }

    public void setPortName(String portName) {
        this.portName = portName;
    }

    public String getRequestFrame() {
        return requestFrame;
    }

    public void setRequestFrame(String requestFrame) {
        this.requestFrame = requestFrame;
    }

    public byte[] getResponseFrame() {
        return responseFrame;
    }

    public void setResponseFrame(byte[] responseFrame) {
        this.responseFrame = responseFrame;
    }

    public String decodeFrame() {
        StringBuilder result = new StringBuilder();
        if ("IS".equalsIgnoreCase(getAtCommand())) {

            if (getRemoteStatus() != 0) {
                return "Status Not Ok {" + getRemoteStatus() + "}";
            } ;
            byte[] bt = getResponseFrame();
            int enabled = getEnabledAnalog(getAnalogMask());

            NumberFormat formatter = NumberFormat.getNumberInstance();
            formatter.setMaximumFractionDigits(3);

            for (int i = 1; i <= enabled; i++) {
                result.append("ad" + i + "=");
                result.append(formatter.format(getVoltageForSample(i)));
                result.append("\n");
            }
        } else {
            result.append("-- -- --");
        }

        return result.toString();
    }

    private int getRemoteStatus() {
        return Frame.unsignedToBytes(getResponseFrame()[17]);
    }

    public int getEnabledAnalog(int analogMask) {
        int enabled = 0;
        enabled = enabled + returnBit(analogMask, 1);
        enabled = enabled + returnBit(analogMask, 2);
        enabled = enabled + returnBit(analogMask, 4);
        enabled = enabled + returnBit(analogMask, 8);
        return enabled;
    }

    public int returnBit(int analogMask, int i) {
        return ((analogMask & i) != 0) ? 1 : 0;
    }

    public double getVoltageForSample(int sampleNum) {
        return getValue(getIntForAnalogSample(getAnalogSample(sampleNum)));
    }

    public byte[] getAnalogSample(int numSample) {
        try {
            int length = getResponseFrame().length;
            int start = length - 1 - 2 * numSample;
            return Arrays.copyOfRange(getResponseFrame(), start, start + 2);
        } catch (Exception e) {
            throw new IllegalStateException("numSample = " + numSample + " : frame : " + PortService.getBytesAsHexString(getResponseFrame()) + " : " + e);
        }
    }

    public int getIntForAnalogSample(byte[] sample) {
        if (sample.length != 2) {
            throw new IllegalArgumentException("sample.length != 2");
        }

        int lsb = Frame.unsignedToBytes(sample[1]);
        int msb = Frame.unsignedToBytes(sample[0]);

        return msb * 256 + lsb;
    }

    public double getValue(int sampleValue) {
        return sampleValue / 1023. * 2.5;
    }

    public int getAnalogMask() {
        return Frame.unsignedToBytes(getResponseFrame()[21]);
    }
}


