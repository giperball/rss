package ru.tusur.rss.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by saturn on 22.09.14.
 */
public class FrameND extends ResponseFrame{
    private static final Logger log = LoggerFactory.getLogger(FrameND.class);
    public FrameND(Frame frame){
        super(frame);
    }
}
