package ru.tusur.rss.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by saturn on 22.09.14.
 */
public class FrameNI extends ResponseFrame{

    private static final Logger log = LoggerFactory.getLogger(FrameNI.class);

    public FrameNI(Frame frame){
        super(frame);
    }

}
