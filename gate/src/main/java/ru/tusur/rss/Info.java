package ru.tusur.rss;

import org.apache.wicket.spring.injection.annot.SpringBean;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by gipreball on 14.11.14.
 */
public class Info {
    private int port;
    private static Info ptr = null;
    private ArrayList<Double> lons;
    private String name;
    private ArrayList<Double> lats;
    private int count_coord = 0;
    private URL url = null;

    private Info(int port) throws IOException {
        this.port = port;
        lons = new ArrayList<Double>();
        lats = new ArrayList<Double>();
        if (port == 8082) {
            url = new URL("http://127.0.0.1:8082/2014_07_14_05.txt");
            name = "MC2";
        }
        else {
            name = "MC1";
            url = new URL("http://127.0.0.1:8081/2014_07_14_03.txt");
        }
    }

    private void readFromFile() throws IOException {

        URLConnection uc = url.openConnection();
        BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream()));
        String line;
        while ((line = br.readLine()) != null) {
            String[] strs = line.split(" ");
            lons.add(Double.parseDouble(strs[15]));
            lats.add(Double.parseDouble(strs[16]));
        }
        br.close();
    }

    public String getPort()
    {
        return Integer.toString(port);
    }

    public String getNextLon() throws IOException {
        if(lats.size() == 0)
            readFromFile();
        count_coord++;
        if(count_coord >= lats.size())
            count_coord = 0;

        return lons.get(count_coord).toString();
    }

    public String getNextLat() throws IOException {
        if(lats.size() == 0)
            readFromFile();
        count_coord++;
        if(count_coord >= lats.size())
            count_coord = 0;

        return lats.get(count_coord).toString();
    }

    public String getName()
    {
        return name;
    }


    public static Info instance()
    {
        return ptr;
    }

    public static void init(int port) throws IOException {
        System.out.println("Info inited, port = " + port);
        ptr = new Info(port);
    }
}
