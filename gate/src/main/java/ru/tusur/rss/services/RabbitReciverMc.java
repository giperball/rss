package ru.tusur.rss.services; /**
 * Created by giperball on 21.10.14.
 */
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

import java.io.IOException;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import javafx.util.Pair;
import org.springframework.stereotype.Service;
import ru.tusur.rss.wicket.data.McHomePage;

import java.io.IOException;
import java.util.HashMap;

//@Service("rabbitReciver")
public class RabbitReciverMc extends Thread {

    public RabbitReciverMc() throws IOException {
    }

    @Override
    public void run() {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setUsername("guest");
            factory.setPassword("guest");
            factory.setVirtualHost("/");
            factory.setHost("127.0.0.1");
            factory.setPort(5672);
            Connection conn = factory.newConnection();
            Channel channel = conn.createChannel();
            String exchangeName = "myExchange";
            String queueName = "myQueue";
            String routingKey = "testRoute";
            boolean durable = true;
            channel.exchangeDeclare(exchangeName, "direct", durable);
            channel.queueDeclare(queueName, durable, false, false, null);
            channel.queueBind(queueName, exchangeName, routingKey);
            QueueingConsumer consumer = new QueueingConsumer(channel);
            channel.basicConsume(queueName, false, consumer);
            boolean run = true;
            while (run) {
                QueueingConsumer.Delivery delivery;
                try {
                    delivery = consumer.nextDelivery();
                    handleDelivery(delivery);
                    channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                } catch (InterruptedException ie) {
                    continue;
                }
            }
            channel.close();
            conn.close();
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
    }

    private void handleDelivery(QueueingConsumer.Delivery delivery)
    {
        System.out.println("New delivery on mc " + new String(delivery.getBody()));
        McHomePage.m_news = new String(delivery.getBody());
    }
}

/*
public class RabbitReciver extends Thread {
    public RabbitReciver() throws IOException {
    }

    @Override
    public void run() {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setUsername("guest");
            factory.setPassword("guest");
            factory.setVirtualHost("/");
            factory.setHost("127.0.0.1");
            factory.setPort(5672);
            Connection conn = factory.newConnection();
            Channel channel = conn.createChannel();
            String exchangeName = "myExchange";
            String queueName = "myQueue";
            String routingKey = "testRoute";
            boolean durable = true;
            channel.exchangeDeclare(exchangeName, "direct", durable);
            channel.queueDeclare(queueName, durable, false, false, null);
            channel.queueBind(queueName, exchangeName, routingKey);
            QueueingConsumer consumer = new QueueingConsumer(channel);
            channel.basicConsume(queueName, false, consumer);
            boolean run = true;
            while (run) {
                QueueingConsumer.Delivery delivery;
                try {
                    delivery = consumer.nextDelivery();
                    System.out.println("New rabbit " + delivery.getBody());
                    channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                } catch (InterruptedException ie) {
                    continue;
                }
            }
            channel.close();
            conn.close();
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
    }
}
*/