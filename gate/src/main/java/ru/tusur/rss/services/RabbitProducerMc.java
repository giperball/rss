package ru.tusur.rss.services;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * Created by giperball on 21.10.14.
 */
@Service("rabbitProducerMc")
public class RabbitProducerMc {
    ConnectionFactory factory;

    public RabbitProducerMc() throws IOException {

        factory = new ConnectionFactory();
        factory.setUsername("guest");
        factory.setPassword("guest");
        factory.setVirtualHost("/");
        factory.setHost("127.0.0.1");
        factory.setPort(5672);
    }

    public void send(String text) throws IOException {
        Connection conn = factory.newConnection();
        Channel channel = conn.createChannel();
//        channel.queueDeclare("myQueue", false, false, false, null);

        String exchangeName = "myExchange1";
        String routingKey = "testRoute1";
        System.out.println("***************************************************************************");
        byte[] messageBodyBytes = text.getBytes();
        channel.basicPublish(exchangeName, routingKey, MessageProperties.PERSISTENT_TEXT_PLAIN, messageBodyBytes);
        channel.close();
        conn.close();

    }
}
/*
public class RabbitProducer {
    ConnectionFactory factory;

    public RabbitProducer() throws IOException {

        factory = new ConnectionFactory();
        factory.setUsername("guest");
        factory.setPassword("guest");
        factory.setVirtualHost("/");
        factory.setHost("127.0.0.1");
        factory.setPort(5672);

    }

    public void send() throws IOException {
        Connection conn = factory.newConnection();
        Channel channel = conn.createChannel();
        String exchangeName = "myExchange";
        String routingKey = "testRoute";

        for (int i=1; i<21; i++) {
            byte[] messageBodyBytes = ("test "+i).getBytes();
            channel.basicPublish(exchangeName, routingKey, MessageProperties.PERSISTENT_TEXT_PLAIN, messageBodyBytes);
        }
        channel.close();
        conn.close();
    }
}
*/