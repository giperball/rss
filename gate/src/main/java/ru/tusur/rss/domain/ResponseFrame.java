package ru.tusur.rss.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by saturn on 22.09.14.
 */
public class ResponseFrame {
    private static final Logger log = LoggerFactory.getLogger(ResponseFrame.class);
    private Frame frame;
    protected String address64;
    public ResponseFrame(Frame frame) {
        this.frame = frame;
        log.info("getFrame()" + getFrame().getAtCommand());
    }

    public Frame getFrame() {
        return frame;
    }

    public String getAddress64() {
        return address64;
    }
}
