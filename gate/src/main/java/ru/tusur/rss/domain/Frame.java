package ru.tusur.rss.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.tusur.rss.services.PortService;

import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Frame {

    private static final Logger log = LoggerFactory.getLogger(Frame.class);


    private byte[] suffix;
    private String niFromNI;

    public byte[] getSuffix() {
        return suffix;
    }

    List<Byte> bytes;
    boolean isNew = true;
    boolean isLength = false;
    boolean isFinished = false;
    boolean isCHK = false;

    public boolean isNew() {
        return isNew;
    }

    public boolean isLength() {
        return isLength;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public boolean isCHK() {
        return isCHK;
    }

    Byte MSB;
    Byte LSB;
    Byte CHK;


    public boolean hasSuffix() {
        return suffix != null && suffix.length != 0;
    }

    private String portName;


    public String getPortName() {
        return portName;
    }

    public Frame(byte[] buffer, String portName) {
        bytes = new LinkedList<>();
        suffix = append(buffer);
        this.portName = portName;
    }


    String hex(byte b) {
        return String.format("%02X ", b);
    }

    public byte[] append(byte[] buffer) {

        int initSize = bytes.size();
        List<Byte> suffix = new ArrayList<>();
        for (byte b : buffer) {

            if (isFinished && !isCHK) {
                if (CHK == null) {
                    CHK = b;
                    bytes.add(b);
                    intBuff = IntBuffer.allocate(bytes.size());
                    for (Byte aByte : bytes) {
                        intBuff.put(unsignedToBytes(aByte));
                    }
                    isCHK = true;
                    continue;
                }
            }

            if (isFinished && isCHK) {
                suffix.add(b);
            }

            if (isNew && b == 0x7E) {
                bytes.add(b);
                isNew = false;
                continue;
            } else if (isNew) {
                log.warn("isNew and first byte not 0x7E ({}) => ignoring", hex(b));
            }

            if (!isNew && !isLength) {
                if (MSB == null) {
                    MSB = b;
                    bytes.add(b);
                    continue;
                }
                if (LSB == null) {
                    LSB = b;
                    bytes.add(b);
                    isLength = true;
                    initCounter();
                    continue;
                } else {
                    throw new IllegalStateException("MSB and LSB not null while !isNew && !isLength");
                }

            }

            if (isLength && !isFinished) {
                bytes.add(b);
                if (payloadCounter == 0) {
                    throw new IllegalStateException("!isFinished and payloadCounter == 0 already");
                }
                payloadCounter--;
                if (payloadCounter == 0) {
                    isFinished = true;
                }
            }
        }

        byte[] result = new byte[suffix.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = suffix.get(i);
        }

        return result;
    }

    public byte[] getFrameBytes() {
        Byte[] bt = bytes.toArray(new Byte[bytes.size()]);
        return bytesFromBytes(bt);
    }

    public static int unsignedToBytes(byte b) {
        return b & 0xFF;
    }

    int payloadCounter;

    private void initCounter() {
        payloadCounter = (MSB << 8) + LSB;
    }

    private IntBuffer intBuff;

    public boolean check() {

        return Frame.check(intBuff);
    }

    public static boolean check(IntBuffer buffer) {

        int sum = 0x00;
        for (int i = 3; i < buffer.capacity(); i++) {
            sum = (sum + buffer.get(i)) & 0xFF;
        }
        return sum == 0xFF;
    }

    public static int countChk(IntBuffer frame) {
        int sum = 0x00;
        for (int i = 3; i < frame.capacity() - 1; i++) {
            sum = (sum + frame.get(i));
        }
        return 0xFF - (sum & 0xFF);
    }


    public int getSize() {
        return (MSB << 8) + LSB;
    }

    public int getDelimiter() {
        return intBuff.get(0);
    }

    public int getFrameType() {
        return intBuff.get(3);
    }

    public int getFrameId() {
        return intBuff.get(4);
    }

    public int getCommandStatus() {
        return intBuff.get(7);
    }

    public String byteToString(Byte[] aBytes) {
        return new String(bytesFromBytes(aBytes));
    }

    public byte[] bytesFromBytes(Byte[] aBytes) {
        byte[] result = new byte[aBytes.length];
        for (int i = 0; i < aBytes.length; i++) {
            result[i] = aBytes[i];

        }
        return result;
    }

    public String getAtCommand() {
        if (getFrameType() == 0x97) {
            return getRemoteAtCommand();
        } else {
            byte[] cmd = {bytes.get(5).byteValue(), bytes.get(6).byteValue()};
            return new String(cmd);
        }
    }

    public String getRemoteAtCommand() {
        byte[] cmd = {bytes.get(15).byteValue(), bytes.get(16).byteValue()};
        return new String(cmd);
    }

    public Byte[] getCommandData() {
        Byte[] bt = bytes.toArray(new Byte[bytes.size()]);
        return Arrays.copyOfRange(bt, 8, bt.length - 1);
    }

    public int getChk() {
        return intBuff.get(intBuff.capacity() - 1);
    }

    public String getNiFromND() {

        return getNi(18);
    }

    public String getAddrFromND() {
        return new String(PortService.getBytesAsHexString(getAddrFromNDbytes()));
    }

    public byte[] getAddrFromNDbytes() {
        return Arrays.copyOfRange(getFrameBytes(), 10, 18);
    }

    public String getNiFromNI() {

        return byteToString(getCommandData());
    }

    private String getNi(int start) {
        int srch = start;
        for (int i = start; i < bytes.size(); i++) {
            byte ch = bytes.get(i);
            if (ch == 0) {
                srch = i;
                break;
            }
        }
        byte[] result = Arrays.copyOfRange(getFrameBytes(), start, srch);
        return new String(result);
    }

    public static IntBuffer getIntBuff(byte[] bytes) {
        IntBuffer result = IntBuffer.allocate(bytes.length);
        for (Byte aByte : bytes) {
            result.put(unsignedToBytes(aByte));
        }
        return result;
    }

    public String getRemoteAddr() {
        return new String(PortService.getBytesAsHexString(getAddrFromRemoteBytes()));
    }

    public byte[] getAddrFromRemoteBytes() {
        return Arrays.copyOfRange(getFrameBytes(), 5, 13);
    }

    @Override
    public String toString() {
        return PortService.getBytesAsHexString(getFrameBytes());
    }
}