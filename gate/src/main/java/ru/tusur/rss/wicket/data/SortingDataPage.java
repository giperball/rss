/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.tusur.rss.wicket.data;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.extensions.markup.html.repeater.data.sort.OrderByBorder;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import ru.tusur.rss.domain.NodeActivity;
import ru.tusur.rss.domain.XBeeNode;
import ru.tusur.rss.services.NodeService;
import ru.tusur.rss.services.PortService;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * page that demonstrates dataview and sorting
 * 
 * @author igor
 * 
 */
public class SortingDataPage extends BaseDataPage
{
	private static final long serialVersionUID = 1L;
    @SpringBean
    NodeService nodeService;

	/**
	 * constructor
	 */
	public SortingDataPage()
	{
        SortableActionsProvider dp = new SortableActionsProvider();
		final DataView<NodeActivity> dataView = new DataView<NodeActivity>("sorting", dp)
		{
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(final Item<NodeActivity> item)
			{
                NodeActivity activity = item.getModelObject();
				item.add(new ActionPanel("actions", item.getModel()));
				item.add(new Label("nodeId", formatNodeId(activity.getNodeId())));
				item.add(new Label("date", formatDate(activity.getUpdateTimeStamp())));
				item.add(new Label("atCommand", activity.getAtCommand()));
				item.add(new Label("nodeName", resolveName(activity)));
				item.add(new Label("decoded", activity.decodeFrame() ));
				item.add(new Label("raw", PortService.getBytesAsHexString(activity.getResponseFrame())));

				item.add(AttributeModifier.replace("class", new AbstractReadOnlyModel<String>()
				{
					private static final long serialVersionUID = 1L;

					@Override
					public String getObject()
					{
						return (item.getIndex() % 2 == 1) ? "even" : "odd";
					}
				}));
			}
		};

		dataView.setItemsPerPage(8L);

		add(new OrderByBorder("orderById", "nodeId", dp)
		{
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSortChanged()
			{
				dataView.setCurrentPage(0);
			}
		});

		add(new OrderByBorder("orderByDate", "updateTime", dp)
		{
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSortChanged()
			{
				dataView.setCurrentPage(0);
			}
		});

		add(dataView);

		add(new PagingNavigator("navigator", dataView));
	}

    private String resolveName(NodeActivity activity) {
        if (nodeService == null) {
            return activity.getNodeName();
        }
        if (StringUtils.isNotEmpty(activity.getNodeName())) {
            return activity.getNodeName();
        } else {
            XBeeNode node = nodeService.getNode(activity.getNodeId());
            return node != null ? node.getNodeName() : activity.getNodeName();
        }
    }

    private String formatNodeId(String nodeId) {
        String result = nodeId;
        if ( nodeId != null && nodeId.length() == 16) {
            result = nodeId.substring(0,8) + " "+ nodeId.substring(8,nodeId.length());
        }
        return result;
    }

    private String formatDate(long millis) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        return sdf.format(new Date(millis));
    }
}
