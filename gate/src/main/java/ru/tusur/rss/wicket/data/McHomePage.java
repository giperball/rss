package ru.tusur.rss.wicket.data;

import com.google.common.base.Ticker;
import org.apache.wicket.ajax.AbstractDefaultAjaxBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.examples.WicketExamplePage;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.string.StringList;
import org.apache.wicket.util.template.PackageTextTemplate;
import ru.tusur.rss.Info;
import ru.tusur.rss.services.RabbitProducerMc;
import sun.misc.IOUtils;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Created by giperball on 21.10.14.
 */

public class McHomePage extends ExamplePage {
    private Label lon_label;
    private Label lat_label;
    private Label m_files_label;
    private String lat_str = "84.952222";
    private String lon_str = "56.488611";
    private Label m_news_label;
    private String m_old_string = "";
    public static String m_news ="";
    public static String m_rabbit_data ="";

    private String m_files = "";
    @SpringBean
    RabbitProducerMc rabbitProducerMc;

    public String scanFolder() throws IOException {

        String result = "";

        File folder = new File("/var/tmp/mc_server/tiny/img/");
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile())
                result += "/tiny/img/" + listOfFiles[i].getName() + ";";
        }

        return result;
    }


    public McHomePage() throws IOException {
        m_news_label = new Label("news", new PropertyModel(this, "m_news"));
        add(new AjaxFallbackLink("link_news") {
            @Override
            public void onClick(AjaxRequestTarget target) {
                boolean flag = false;
                if (!m_old_string.equals(m_news)) {
                    m_old_string = m_news;
                    flag = true;
                }



                if (target != null && flag == true) {
                    target.add(m_news_label);

                }
            }
        });
        lat_label = new Label("lat", new PropertyModel(this, "lat_str"));
        lon_label = new Label("lon", new PropertyModel(this, "lon_str"));
        m_files_label = new Label("files", new PropertyModel(this, "m_files"));
        lat_label.setOutputMarkupId(true);
        lon_label.setOutputMarkupId(true);
        m_files_label.setOutputMarkupId(true);
        add(lon_label);
        add(lat_label);
        add(m_files_label);

        m_files = scanFolder();


        add(new AjaxFallbackLink("coord_id") {

                @Override
                public void onClick(AjaxRequestTarget target) {
                    /**/

                    try {
                        m_files = scanFolder();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    /**/
                    try {
                        lat_str = Info.instance().getNextLat();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        lon_str = Info.instance().getNextLon();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (target != null) {
                        try {
                            rabbitProducerMc.send("send_coords " +
                                    Info.instance().getName() +
                            " " +
                            lat_str +
                            " " +
                            lon_str);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        target.add(lon_label);
                        target.add(lat_label);
                        target.add(m_files_label);
                    }
                }
            }
        );
        m_news_label.setOutputMarkupId(true);
        add(m_news_label);
    }

}
