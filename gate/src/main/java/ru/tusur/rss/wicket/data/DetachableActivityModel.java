/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.tusur.rss.wicket.data;

import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tusur.rss.dao.ActivitiesRepository;
import ru.tusur.rss.domain.NodeActivity;

/**
 * detachable model for an instance of contact
 * 
 * @author igor
 * 
 */
public class DetachableActivityModel extends LoadableDetachableModel<NodeActivity>
{

    @Autowired
    @SpringBean
    ActivitiesRepository activitiesRepository;

    private final long id;

	protected ActivitiesRepository getContactsDB()
	{
		return GateDatabaseLocator.getDatabase();
	}

	/**
	 * @param c
	 */
	public DetachableActivityModel(NodeActivity c)
	{
		this(c.getId());
	}

	/**
	 * @param id
	 */
	public DetachableActivityModel(long id)
	{
		if (id == 0)
		{
			throw new IllegalArgumentException();
		}
		this.id = id;
	}

	/**
	 * @see Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return Long.valueOf(id).hashCode();
	}

	/**
	 * used for dataview with ReuseIfModelsEqualStrategy item reuse strategy
	 *
	 * @see org.apache.wicket.markup.repeater.ReuseIfModelsEqualStrategy
	 * @see Object#equals(Object)
	 */
	@Override
	public boolean equals(final Object obj)
	{
		if (obj == this)
		{
			return true;
		}
		else if (obj == null)
		{
			return false;
		}
		else if (obj instanceof DetachableActivityModel)
		{
			DetachableActivityModel other = (DetachableActivityModel)obj;
			return other.id == id;
		}
		return false;
	}

	/**
	 * @see org.apache.wicket.model.LoadableDetachableModel#load()
	 */
	@Override
	protected NodeActivity load()
	{
		// loads contact from the database
//		return activitiesRepository.findOne((int)id);
		return getContactsDB().findOne((int)id);
	}
}
